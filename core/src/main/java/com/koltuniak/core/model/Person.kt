package com.koltuniak.bartosz.skieletonprojects.data.model

interface PersonInterface {
    val name: String
    val lastName: String
}