package com.koltuniak.core.model

data class RestConfiguration(val url: String)