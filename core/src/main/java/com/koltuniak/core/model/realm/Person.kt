package com.koltuniak.bartosz.skieletonprojects.data.model.realm

import io.realm.RealmObject

open class Person(
    override var name: String = "",
    override var lastName: String = ""
) : RealmObject(),
    com.koltuniak.bartosz.skieletonprojects.data.model.PersonInterface

