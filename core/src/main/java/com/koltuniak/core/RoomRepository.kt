//package com.example.core
//
//import androidx.room.*
//import androidx.sqlite.db.SimpleSQLiteQuery
//import androidx.sqlite.db.SupportSQLiteQuery
//import com.example.bartosz.skieletonprojects.base.Architecture
//import com.example.bartosz.skieletonprojects.data.model.realm.Person
//import io.reactivex.Completable
//import io.reactivex.Observable
//
//@Database(entities = arrayOf(Person::class), version = 1)
//abstract class RoomRepository(val daoRoom: DaoRoom) : RoomDatabase(), Architecture.LocalRepository {
//
//
//    override fun <T> saveData(list: List<T>): Completable {
//        val insertResult = daoRoom.insert(list)
//        return insertResult
//    }
//
//    @Transaction
//    override fun <T> saveData(item: T) {
//        val id: Long = daoRoom.insert(item)
//        if (id == -1L) {
//            daoRoom.update(item)
//        }
//    }
//
//    override fun <T> getListObservable(t: Class<T>): Observable<List<T>> {
//        val query =
//            SimpleSQLiteQuery("select * from " + t.name)
//        return daoRoom.doFindAllValid(query)
//    }
//
//
//
////    abstract fun <T> getObservable(t: Class<T>): Observable<T>
//
////    override fun init()
//
////    override fun deInit()
//
//    override fun registerTypes(classInterface: Class<*>, classRealm: Class<*>) {
//
//    }
//
//}
//
//@Dao
//interface DaoRoom {
//
//    /**
//     * Insert an object in the database.
//     *
//     * @param obj the object to be inserted.
//     * @return The SQLite row id
//     */
//    @Insert(onConflict = OnConflictStrategy.IGNORE)
//    abstract fun <T> insert(obj: T?): Long
//
//    /**
//     * Insert an array of objects in the database.
//     *
//     * @param obj the objects to be inserted.
//     * @return The SQLite row ids
//     */
//    @Insert(onConflict = OnConflictStrategy.IGNORE)
//    abstract fun <T> insert(obj: List<T?>?): Completable
//
//    /**
//     * Update an object from the database.
//     *
//     * @param obj the object to be updated
//     */
//    @Update
//    abstract fun <T> update(obj: T?)
//
//    /**
//     * Update an array of objects from the database.
//     *
//     * @param obj the object to be updated
//     */
//    @Update
//    abstract fun <T> update(obj: List<T?>?)
//
//    @RawQuery
//    abstract fun <T> doFindAllValid(query: SupportSQLiteQuery?): Observable<List<T>>
//}
