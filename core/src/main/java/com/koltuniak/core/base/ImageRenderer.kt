package com.koltuniak.core.base

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView

interface ImageRenderer {
    fun render(
        imageView: ImageView,
        imageUrl: String,
        onLoadFailedListener: (() -> Unit) = {},
        onResourceReadyListener: ((drawable: Drawable) -> Unit) = {}
    )

}