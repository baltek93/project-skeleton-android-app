package com.koltuniak.core.base

import com.koltuniak.core.BuildConfig
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

interface DisposableSingleObserver<T> : SingleObserver<T>, Disposable {
    override fun onError(e: Throwable) {
        if (BuildConfig.DEBUG)
            throw e
        else
            BaseLogger.log(e.toString())
    }
}