package com.koltuniak.core.base

import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.bartosz.skieletonprojects.data.model.PersonInterface
import com.squareup.moshi.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.realm.*
import java.lang.reflect.Type

@Suppress("UNCHECKED_CAST")
class RealmRepository(private val realmConfiguration: RealmConfiguration) :
    Architecture.LocalRepository {

    lateinit var realm: Realm

    override fun <T> getObservable(t: Class<T>): Observable<T> {
        return Observable.create { emitter ->
            val articles = where(t).findFirstAsync() as T
            if (articles != null) {
                emitter.onNext(articles)
                emitter.onComplete()
            } else {
                emitter.onError(IndexOutOfBoundsException())
            }
        }
    }

    override fun <T, T2> getObservable(t: Class<T>, key: String?, value: T2?): Observable<T> {
        val where = where(t)
        if (key != null && value != null) {
            when (value) {
                is String -> where.equalTo(key, value)
                is Boolean -> where.equalTo(key, value)
                is Int -> where.equalTo(key, value)
                is Long -> where.equalTo(key, value)
            }
        }
        return where
            .findAllAsync()
            .asFlowable()
            .filter { it.isLoaded }
            .map {
                if (it.isNotEmpty()) it[0] else null
            }.toObservable() as Observable<T>


    }

    override fun init() {
        realm = Realm.getInstance(realmConfiguration)

    }

    override fun deInit() {
        realm.close()
    }

    override fun <T> getListObservable(t: Class<T>): Observable<List<T>> {
        var result = where(t)

        return result.findAllAsync()
            .asFlowable()
            .filter { it.isLoaded }
            .map {
                it as List<T>
            }.toObservable().observeOn(AndroidSchedulers.mainThread())
    }

    private fun where(t: Class<*>): RealmQuery<RealmModel> {
        return realm.where(getProperType(t))
    }

    fun <T> getProperType(inputType: Class<T>): Class<RealmModel> = when {
        hashMap.containsKey(inputType) -> hashMap[inputType]!! as Class<RealmModel>
        hashMap.containsValue(inputType) -> inputType as Class<RealmModel>
        else -> inputType as Class<RealmModel>
    }


    override fun <T> saveData(item: T) {
        realm.executeTransaction {
            saveWithProperType(item, it)
        }
    }

    var hashMap = mutableMapOf<Class<*>, Class<*>>(
        Pair(
            PersonInterface::class.java,
            com.koltuniak.bartosz.skieletonprojects.data.model.realm.Person::class.java
        )
    )

    override fun registerTypes(classInterface: Class<*>, classRealm: Class<*>) {
        if (!hashMap.containsKey(classInterface)) {
            hashMap[classInterface] = classRealm
        }
    }


    fun closeRealm() {
        realm.close()
    }

    override fun <T> saveData(list: List<T>) {
        realm.executeTransactionAsync {
            saveWithProperType(list, it)
        }

    }

    private fun <T> saveWithProperType(obj: T, it: Realm) {
        when (obj) {
            is List<*> -> it.copyToRealmOrUpdate(obj as List<RealmObject>)
            is RealmObject -> it.copyToRealmOrUpdate(obj as RealmObject)
            else -> throw UnsupportedOperationException("Object is not RealmObject or RealmList")
        }
    }


    class RealmListJsonAdapterFactory : JsonAdapter.Factory {

        override fun create(
            type: Type,
            annotations: Set<Annotation>,
            moshi: Moshi
        ): JsonAdapter<*>? {
            val rawType = Types.getRawType(type)
            if (!annotations.isEmpty()) return null
            return if (rawType == RealmList::class.java) {
                newRealmListAdapter<RealmObject>(type, moshi).nullSafe()
            } else null
        }

        private fun <T : RealmObject> newRealmListAdapter(
            type: Type,
            moshi: Moshi
        ): JsonAdapter<RealmList<T>> {
            val elementType = Types.collectionElementType(type, RealmList::class.java)
            val elementAdapter = moshi.adapter<T>(elementType)
            return RealmListAdapter(elementAdapter)
        }
    }

    class RealmListAdapter<T : RealmModel>(
        val elementAdapter: JsonAdapter<T>
    ) : JsonAdapter<RealmList<T>>() {
        override fun toJson(writer: JsonWriter, value: RealmList<T>?) {
            writer.beginArray()
            if (value != null) {
                for (element in value) {
                    elementAdapter.toJson(writer, element)
                }
            }
            writer.endArray()
        }

        companion object {

            val FACTORY: Factory = Factory { type, _, moshi ->
                val rawType: Class<*> = Types.getRawType(type)

                if (rawType == RealmList::class.java) {
                    val elementType = Types.collectionElementType(type, RealmList::class.java)
                    val elementAdapter = moshi.adapter<RealmModel>(elementType)
                    return@Factory RealmListAdapter(elementAdapter).nullSafe()
                }
                null
            }
        }

        override fun fromJson(reader: JsonReader): RealmList<T> {

            val result = RealmList<T>()
            reader.beginArray()
            while (reader.hasNext()) {
                result.add(elementAdapter.fromJson(reader))
            }
            reader.endArray()
            return result
        }
    }

}