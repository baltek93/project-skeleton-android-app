package com.koltuniak.core.base

import io.realm.annotations.RealmModule


@RealmModule(library = true, allClasses = true)
open class BaseModuleRealm