package com.koltuniak.bartosz.skieletonprojects.base

import androidx.lifecycle.LifecycleOwner
import io.reactivex.Observable

interface Architecture {
    interface LocalRepository {

        fun <T> saveData(list: List<T>)
        fun <T> saveData(item: T)

        fun <T> getListObservable(t: Class<T>): Observable<List<T>>
        fun <T> getObservable(t: Class<T>): Observable<T>
        fun <T, T2> getObservable(
            t: Class<T>,
            key: String? = null,
            value: T2? = null
        ): Observable<T>

        fun init()
        fun deInit()

        fun registerTypes(classInterface: Class<*>, classRealm: Class<*>)


    }

    interface BaseViewModel<STATE> {
        fun observe(
            lifecycleOwner: LifecycleOwner,
            observer: com.koltuniak.core.base.BaseViewModel.BaseObserver<STATE>
        )

        fun stopObserving(observer: com.koltuniak.core.base.BaseViewModel.BaseObserver<STATE>)
        fun getState(): STATE


    }


    interface BaseView {
        val stateMenu: StateMenu
    }

    enum class StateMenu {
        SHOW, HIDE
    }

    interface BaseActivity {
        fun toggleMenu(stateMenu: StateMenu)
    }

    interface BaseUseCase {

    }

}


