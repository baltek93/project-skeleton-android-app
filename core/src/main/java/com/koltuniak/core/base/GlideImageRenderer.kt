package com.koltuniak.core.base

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

class GlideImageRenderer(val context: Context) : ImageRenderer {
    override fun render(
        imageView: ImageView,
        imageUrl: String,
        onLoadFailedListener: () -> Unit,
        onResourceReadyListener: (drawable: Drawable) -> Unit
    ) {
        GlideApp.with(context)
            .load(imageUrl)
            .fitCenter()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?, model: Any?, target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    onLoadFailedListener.invoke()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?, model: Any?, target: Target<Drawable>?,
                    dataSource: DataSource?, isFirstResource: Boolean
                ): Boolean {
                    resource?.let { onResourceReadyListener.invoke(it) }
                    return false
                }
            })
            .into(imageView)
    }


}