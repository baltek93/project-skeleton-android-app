package com.koltuniak.core.base

import android.util.Log
import com.koltuniak.core.BuildConfig
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver

class ViewModelCompletableObserver(
    val viewModel: BaseViewModel<out Any>,
    private val onComplete: (() -> Unit)? = null,
    private val onError: ((e: Throwable) -> Unit)? = null

) :
    DisposableCompletableObserver() {
    override fun onComplete() {
        onComplete?.invoke()
    }


    override fun onError(e: Throwable) {
        onError?.invoke(e)
        logError(e)
    }
}

class ViewModelObserver<T>(
    val viewModel: BaseViewModel<out Any>,
    private val onNext: ((T) -> Unit)? = null
) : DisposableObserver<T>() {
    override fun onError(e: Throwable) {
        logError(e)
    }

    override fun onNext(t: T) {
        onNext?.invoke(t)
    }

    override fun onComplete() {}
}

private fun logError(e: Throwable) {
    if (BuildConfig.DEBUG) {
        Log.e("ErrorResult", "error: ${e.message}", e)
    }
    Log.v("ErrorResult: ", e?.message?: "")
}