package com.koltuniak.core.base

import android.app.Application
import androidx.lifecycle.*
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.koin.core.component.KoinComponent


abstract class BaseViewModel<STATE> : ViewModel(), Architecture.BaseViewModel<STATE>,
    KoinComponent {

    private val model = MutableLiveData<STATE>()
    protected val disposables = CompositeDisposable()

    override fun getState(): STATE {
        return model.value ?: getDefaultState()

    }

    override fun observe(lifecycleOwner: LifecycleOwner, observer: BaseObserver<STATE>) {
        model.observe(lifecycleOwner, observer)
    }

    override fun stopObserving(observer: BaseObserver<STATE>) {
        model.removeObserver(observer)
    }

    abstract fun getDefaultState(): STATE

    protected fun setState(model: STATE) {
        this.model.value = model
    }


    fun <T> Single<T>.subscribeOnBackgroundScheduler(subscriber: DisposableSingleObserver<T>) {
        subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(subscriber)
        addToDisposables(subscriber)
    }

    fun <T> Observable<T>.subscribeOnMainScheduler(observer: DisposableObserver<T>) {
        val disposable = observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer)
        addToDisposables(disposable)
    }

    fun Completable.subscribeOnMainScheduler(observer: Action) {
        val disposable = observeOn(AndroidSchedulers.mainThread())
            .subscribe(observer)
        addToDisposables(disposable)
    }

    protected fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    open fun clear() {
        disposables.clear()
    }

    override fun onCleared() {
        clear()
        super.onCleared()
    }

    interface BaseObserver<STATE> : Observer<STATE> {

        override fun onChanged(t: STATE) {
            if (t != null)
                onViewStateChanged(t)
        }

        fun onViewStateChanged(state: STATE)

    }
}
