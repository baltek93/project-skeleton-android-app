package com.koltuniak.core.base

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.reflect.InvocationTargetException

class ParametrizedViewModelFactory<P : Any>(
    private val app: Application,
    private val viewModelParameter: P
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val paramClass =
            viewModelParameter::class.javaPrimitiveType ?: viewModelParameter::class.java
        try {
            return modelClass
                .getConstructor(Application::class.java, paramClass)
                .newInstance(app, viewModelParameter)
        } catch (e: NoSuchMethodException) {
            throw RuntimeException("Cannot create an instance of $modelClass", e)
        } catch (e: IllegalAccessException) {
            throw RuntimeException("Cannot create an instance of $modelClass", e)
        } catch (e: InstantiationException) {
            throw RuntimeException("Cannot create an instance of $modelClass", e)
        } catch (e: InvocationTargetException) {
            throw RuntimeException("Cannot create an instance of $modelClass", e)
        }
    }
}