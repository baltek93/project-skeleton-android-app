package com.koltuniak.core.base

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders


fun <V : BaseViewModel<*>, P : Any> viewModelOf(
    fragment: Fragment, viewModelClass: Class<V>, viewModelParameter: P
): V {
    return ViewModelProviders.of(
        fragment,
        ParametrizedViewModelFactory(fragment.requireActivity().application, viewModelParameter)
    ).get(viewModelClass)
}

fun <V : BaseViewModel<*>, P : Any> viewModelOf(
    activity: FragmentActivity, viewModelClass: Class<V>, viewModelParameter: P
): V {
    return ViewModelProviders.of(
        activity,
        ParametrizedViewModelFactory(activity.application, viewModelParameter)
    ).get(viewModelClass)
}

fun <V : BaseViewModel<*>> viewModelOf(fragment: Fragment, viewModelClass: Class<V>): V {
    return ViewModelProviders.of(fragment).get(viewModelClass)
}

fun <V : BaseViewModel<*>> viewModelOf(activity: FragmentActivity, viewModelClass: Class<V>): V {
    return ViewModelProviders.of(activity).get(viewModelClass)
}

fun Activity.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = currentFocus
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}