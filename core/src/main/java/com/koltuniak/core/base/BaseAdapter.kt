package com.koltuniak.core.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.koltuniak.core.base.ViewType.Companion.ITEM_TYPE
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit


class BaseAdapter() :
    RecyclerView.Adapter<BaseAdapter.BaseViewHolder>() {

    private lateinit var renderable: Renderable
    private val itemClickProcessor: PublishProcessor<Pair<View, Int>> = PublishProcessor.create()
    private val disposables = CompositeDisposable()
    private val viewTypes = mutableMapOf<Int, ViewType>()
    fun render(renderable: Renderable) {
        this.renderable = renderable
        this.notifyDataSetChanged()
    }

    fun isInitialized(): Boolean {
        return ::renderable.isInitialized
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        itemClickProcessor
            .throttleFirst(1, TimeUnit.SECONDS)
            .subscribeBy { (view, position) ->
                renderable.onItemClick(view, position)
            }
            .addTo(disposables)

    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        disposables.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(viewTypes[viewType]!!.layoutId, parent, false)
        return BaseViewHolder(view, viewType = viewType)
    }

    override fun getItemCount(): Int {
        if (!isInitialized()) return -1
        return renderable.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        renderable.bindView(holder.itemView, position)
        holder.view.setOnClickListener {
            itemClickProcessor.onNext(Pair(it, position))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return renderable.getViewTypeForPos(position)
    }


    class BaseViewHolder(val view: View, viewType: Int) : RecyclerView.ViewHolder(view)


    fun setLayout(layoutId: Int) {
        this.viewTypes[ITEM_TYPE] = ViewType(ITEM_TYPE, layoutId)
    }

    fun setViewTypes(viewTypes: List<ViewType>) {
        this.viewTypes.clear()
        this.viewTypes.putAll(viewTypes.map { Pair(it.type, it) })
    }
}

interface Renderable {
    val size: Int
    fun bindView(view: View, position: Int)
    fun onItemClick(item: View, position: Int) {}
    fun getViewTypeForPos(position: Int): Int {
        return ViewType.ITEM_TYPE
    }

}

data class ViewType(
    val type: Int, @LayoutRes val layoutId: Int = -1
) {
    companion object {
        val HEADER_TYPE = -2
        val FOOTER_TYPE = -3
        val ITEM_TYPE = 0
    }
}