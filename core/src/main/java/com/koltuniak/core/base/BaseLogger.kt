package com.koltuniak.core.base

import com.orhanobut.logger.Logger
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class BaseLogger {

    companion object {
        fun log(msg: String) {
            Logger.d("", msg)
        }

        fun logJson(msg: String) {
            Logger.json(msg)
        }
    }
}

class JsonLogger : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        if (isMessageJson(message))
            BaseLogger.logJson(message)
        else HttpLoggingInterceptor.Logger.DEFAULT.log(message)
    }

    private fun isMessageJson(message: String): Boolean {
        return try {
            val json = message.trim { it <= ' ' }
            when {
                json.startsWith("{") -> {
                    JSONObject(json)
                    true
                }
                json.startsWith("[") -> {
                    JSONArray(json)
                    true
                }
                else -> false
            }

        } catch (e: JSONException) {
            false
        }
    }

}
