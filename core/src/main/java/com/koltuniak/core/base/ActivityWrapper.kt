package com.koltuniak.core.base

import androidx.appcompat.app.AppCompatActivity

data class ActivityWrapper(var activity: AppCompatActivity?)