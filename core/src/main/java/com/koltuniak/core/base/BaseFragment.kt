package com.koltuniak.core.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import io.reactivex.disposables.CompositeDisposable


abstract class BaseFragment<STATE : Any, V : Architecture.BaseViewModel<STATE>> : Fragment(),
    LifecycleObserver,
    BaseViewModel.BaseObserver<STATE>, Architecture.BaseView {
    open val mainViewModel: V? = null

    private val disposables = CompositeDisposable()
    private var lifecycleOwner: ViewLifecycleOwner? = null
    override val stateMenu: Architecture.StateMenu = Architecture.StateMenu.HIDE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true


    }

    fun observe(lifecycleOwner: LifecycleOwner) {
        mainViewModel?.observe(lifecycleOwner, this)
//        viewModel.observeEvents(lifecycleOwner, ViewStateEventObserver(this))
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleOwner = ViewLifecycleOwner()
        lifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        setLifecycleObserver()
        init()
    }


    open fun init() {

    }

    fun setMenuVisible() {

    }

    override fun onStart() {
        super.onStart()
        lifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        (activity as Architecture.BaseActivity).toggleMenu(stateMenu)
        lifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        lifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        super.onPause()
    }

    override fun onStop() {
        lifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
        super.onStop()
    }

    override fun onDestroyView() {
        lifecycleOwner?.lifecycle?.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        removeLifecycleObserver()
        super.onDestroyView()
    }

    private fun setLifecycleObserver() {
        lifecycleOwner?.let {
            it.lifecycle.addObserver(this)
            (this as BaseFragment<*, *>).observe(this)

        }
    }

    private fun removeLifecycleObserver() {
        lifecycleOwner?.lifecycle?.removeObserver(this)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unsubscribe() {
        disposables.clear()
        mainViewModel?.stopObserving(this)
    }

    inner class ViewLifecycleOwner : LifecycleOwner {

        private val lifecycleRegistry = LifecycleRegistry(this)

        override fun getLifecycle(): LifecycleRegistry {
            return lifecycleRegistry
        }
    }

}