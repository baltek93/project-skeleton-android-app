package com.koltuniak.core.base

import android.app.Application
import android.content.Context
import android.content.pm.ApplicationInfo
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.bartosz.skieletonprojects.base.Architecture.LocalRepository
import com.koltuniak.core.baseDependency
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger

open class BaseApplication : Application() {

    private var refWatcher: RefWatcher? = null
    lateinit var repository: LocalRepository

    companion object {
        fun getRefWatcher(context: Context): RefWatcher? {
            val application = context.applicationContext as BaseApplication
            return application.refWatcher
        }

        val isRunningTest: Boolean by lazy {
            try {
                Class.forName("android.support.test.espresso.Espresso")
                true
            } catch (e: Throwable) {
                false
            }
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        Logger.addLogAdapter(
            AndroidLogAdapter(
                PrettyFormatStrategy.newBuilder()
                    .tag("Base")
                    .methodCount(7)
                    .build()

            )
        )
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
//       This process is dedicated to LeakCanary for heap analysis.
//       You should not init your app in this process.
            return
        }
        startKoin()
        initRealm()



        //if (BuildConfig.DEBUG && !isRunningTest)
        if (applicationContext.applicationInfo.flags.and(ApplicationInfo.FLAG_DEBUGGABLE) != 0 && !isRunningTest)
            refWatcher = LeakCanary.install(this)
    }

    open fun startKoin() {
        startKoin {

            androidContext(this@BaseApplication)
            modules(baseDependency)
            logger(EmptyLogger())
        }
    }

    private fun initRealm() {
        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder().modules(BaseModuleRealm()).deleteRealmIfMigrationNeeded()
                .build()
        )
        val repository: LocalRepository by inject()
        repository.init()
    }


}