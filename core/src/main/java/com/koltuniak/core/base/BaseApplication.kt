package com.koltuniak.bartosz.skieletonprojects.base

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.pm.ApplicationInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.koltuniak.bartosz.skieletonprojects.base.Architecture.LocalRepository
import com.koltuniak.core.base.ActivityWrapper
import com.koltuniak.core.base.BaseActivityLifecycleCallbacks
import com.koltuniak.core.base.BaseModuleRealm
import com.koltuniak.core.base.injector
import com.koltuniak.core.baseDependency
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import io.realm.Realm
import io.realm.RealmConfiguration
import org.kodein.di.Kodein
import org.kodein.di.direct
import org.kodein.di.generic.instance

open class BaseApplication : Application() {

    private var refWatcher: RefWatcher? = null
    lateinit var repository: LocalRepository

    companion object {
        fun getRefWatcher(context: Context): RefWatcher? {
            val application = context.applicationContext as BaseApplication
            return application.refWatcher
        }

        val isRunningTest: Boolean by lazy {
            try {
                Class.forName("android.support.test.espresso.Espresso")
                true
            } catch (e: Throwable) {
                false
            }
        }
    }

    open var kodein: Kodein = Kodein {
        import(baseDependency)
    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        Logger.addLogAdapter(
            AndroidLogAdapter(
                PrettyFormatStrategy.newBuilder()
                    .tag("Base")
                    .methodCount(7)
                    .build()

            )
        )
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
//       This process is dedicated to LeakCanary for heap analysis.
//       You should not init your app in this process.
            return
        }
        initRealm()

        registerActivityLifecycleCallbacks(object : BaseActivityLifecycleCallbacks {
            override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
                if (p0 is AppCompatActivity) {
                    val activityWrapper by kodein.instance<ActivityWrapper>()
                    activityWrapper.activity = p0
                }
            }

            override fun onActivityStarted(p0: Activity?) {
                if (p0 is AppCompatActivity) {
                    val activityWrapper by kodein.instance<ActivityWrapper>()
                    activityWrapper.activity = p0
                }
            }

            override fun onActivityDestroyed(p0: Activity?) {
                repository.deInit()
                val wrapper by kodein.instance<ActivityWrapper>()
                if (wrapper.activity == p0) {
                    wrapper.activity = null
                }
            }
        })
        //if (BuildConfig.DEBUG && !isRunningTest)
        if (applicationContext.applicationInfo.flags.and(ApplicationInfo.FLAG_DEBUGGABLE) != 0 && !BaseApplication.isRunningTest)
            refWatcher = LeakCanary.install(this)
    }

    private fun initRealm() {
        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder().modules(BaseModuleRealm()).deleteRealmIfMigrationNeeded()
                .build()
        )
        repository = injector(context = this).direct.instance<Architecture.LocalRepository>()
        repository.init()
    }


}