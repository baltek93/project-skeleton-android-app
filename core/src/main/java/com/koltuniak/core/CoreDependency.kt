package com.koltuniak.core

import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.core.base.GlideImageRenderer
import com.koltuniak.core.base.JsonLogger
import com.koltuniak.core.base.RealmRepository
import com.koltuniak.core.model.RestConfiguration
import com.squareup.moshi.Moshi
import io.realm.Realm
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


val baseDependency = module {


    factory<RealmConfiguration> {
        RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .build()
    }
    factory<RealmRepository> { params ->
        Realm.init(params.get())
        RealmRepository(get())
    }


    single<Architecture.LocalRepository> {
        RealmRepository(get())
    }


    single<RestConfiguration> {
        RestConfiguration(url = "http://localhost//wordpress/")
    }

    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl(get<RestConfiguration>().url)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(
                MoshiConverterFactory
                    .create(
                        Moshi.Builder().add(RealmRepository.RealmListJsonAdapterFactory()).build()
                    )
            )
            .client(get())
            .build()
    }

    single<OkHttpClient> {
        HttpUtils.addAllTrustingCertificateManager(get())
    }

    single<OkHttpClient.Builder> {
        val log = HttpLoggingInterceptor(JsonLogger())
        log.level = HttpLoggingInterceptor.Level.BODY

//        val headerInterceptor: Architecture.HeaderInterceptor = instance()

        OkHttpClient.Builder()
//            .addInterceptor(headerInterceptor)
            .addInterceptor(log)
    }

    factory { params -> GlideImageRenderer(params.get()) }

}