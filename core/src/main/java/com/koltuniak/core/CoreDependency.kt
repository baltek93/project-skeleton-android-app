package com.koltuniak.core

import android.content.Context
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.core.base.*
import com.koltuniak.core.model.RestConfiguration
import com.squareup.moshi.Moshi
import io.realm.Realm
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein.Module
import org.kodein.di.generic.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


val baseDependency = Module {


    bind<RealmConfiguration>() with provider {
        RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .build()
    }
    bind<RealmRepository>() with multiton { context: Context ->
        Realm.init(context)
        RealmRepository(instance())
    }


    bind<Architecture.LocalRepository>() with singleton {
        RealmRepository(instance())
    }

    bind<RestConfiguration>() with instance(RestConfiguration(url = "http://localhost//wordpress/"))

    bind<Retrofit>() with singleton {
        Retrofit.Builder()
            .baseUrl(instance<RestConfiguration>().url)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(
                MoshiConverterFactory
                    .create(
                        Moshi.Builder().add(RealmRepository.RealmListJsonAdapterFactory()).build()
                    )
            )
            .client(instance())
            .build()
    }
    bind<OkHttpClient>() with singleton {
        HttpUtils.addAllTrustingCertificateManager(instance())
//        instance<OkHttpClient.Builder>().build()
    }

    bind<OkHttpClient.Builder>() with singleton {
        val log = HttpLoggingInterceptor(JsonLogger())
        log.level = HttpLoggingInterceptor.Level.BODY

//        val headerInterceptor: Architecture.HeaderInterceptor = instance()

        OkHttpClient.Builder()
//            .addInterceptor(headerInterceptor)
            .addInterceptor(log)
    }


    bind<ActivityWrapper>() with singleton {
        ActivityWrapper(null)
    }

    bind<ImageRenderer>() with factory { context: Context ->
        GlideImageRenderer(context)
    }

}