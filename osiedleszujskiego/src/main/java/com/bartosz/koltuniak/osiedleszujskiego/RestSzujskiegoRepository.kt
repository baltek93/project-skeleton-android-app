package com.koltuniak.bartosz.skieletonprojects.post.data

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm.ImageModel
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm.PostModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RestSzujskiegoRepository {

    @GET("wp-json/wl/v2/posts")
    fun getPosts(@Query("numberposts") postsAmount: Int? = null): Single<List<PostModel>>

    @GET("wp-json/wl/v2/gallery")
    fun getGallery(@Query("numberImage") imageAmount: Int? = null): Single<List<ImageModel>>
}