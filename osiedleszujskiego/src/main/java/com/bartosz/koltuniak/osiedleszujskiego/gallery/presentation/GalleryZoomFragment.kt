package com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.bartosz.koltuniak.osiedleszujskiego.splash.LinearLayoutManagerWithSmoothScroller
import com.koltuniak.core.base.*
import kotlinx.android.synthetic.main.fragment_gallery_zoom.*
import kotlinx.android.synthetic.main.gallery_item_zoom.view.*
import kotlinx.android.synthetic.main.item_headers_image.view.*
import org.koin.android.ext.android.inject


class GalleryZoomFragment :
    BaseFragment<GalleryContract.GalleryZoomViewState, GalleryContract.GalleryZoomViewModel>(),
    GalleryContract.GalleryZoomView {
    override lateinit var mainViewModel: GalleryContract.GalleryZoomViewModel
    private val viewAdapter = BaseAdapter()

    private val imageRenderer: ImageRenderer by inject()


    override fun init() {
        super.init()
        viewAdapter.setLayout(R.layout.gallery_item_zoom)
        val itemId = arguments?.getString(PostContract.ITEM_ID)
        mainViewModel.setFirstSelectPosition(itemId)
        initListAdapter()
        back.setOnClickListener {
            findNavController().popBackStack()
        }

        mainImageViewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                changeItem(position, mainViewModel.getState())
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_gallery_zoom, container, false)
    }

    override fun onViewStateChanged(state: GalleryContract.GalleryZoomViewState) {
        renderList(state)
        mainImageViewPager.adapter =
            GalleryPagerAdapter(this.requireContext(), state, imageRenderer)
        mainImageViewPager.currentItem = state.selectionPosition
        galleryZoomList.layoutManager?.scrollToPosition(state.selectionPosition)
    }


    private fun initListAdapter() {
        val viewManager =
            LinearLayoutManagerWithSmoothScroller(context, LinearLayoutManager.HORIZONTAL, false)
        galleryZoomList.layoutManager = viewManager
        galleryZoomList.adapter = viewAdapter
        renderList(GalleryContract.GalleryZoomViewState())

    }

    private fun renderList(state: GalleryContract.GalleryZoomViewState) {
        viewAdapter.render(object : Renderable {
            override val size = state.images.size

            override fun onItemClick(item: View, position: Int) {
                val image = state.images[position]
                mainViewModel.setFirstSelectPosition(image.id)
                changeItem(position, state)
//                image.large.replace("localhost", "192.168.1.19")
//                findNavController().navigate(R.id.gallery_to_galleryZoom)
            }

            override fun bindView(view: View, position: Int) {
                val image = state.images[position]
                val imageUrl = image.large.replace("localhost", "192.168.1.19")
                if (position == state.selectionPosition) {
                    view.galleryZoomImage.background = context?.getDrawable(R.drawable.border)
                } else {
                    view.galleryZoomImage.background = null
                }
                imageRenderer.render(view.galleryZoomImage, imageUrl, onResourceReadyListener = {
                    view.galleryZoomImage.setImageDrawable(it)
                })
            }

        })
    }

    private fun changeItem(
        position: Int,
        state: GalleryContract.GalleryZoomViewState
    ) {
        viewAdapter.notifyItemChanged(mainViewModel.getState().selectionPosition)
        mainViewModel.getState().selectionPosition = position
        viewAdapter.notifyItemChanged(position)
        mainImageViewPager.currentItem = state.selectionPosition
        galleryZoomList.smoothScrollToPosition(position)
    }

    inner class GalleryPagerAdapter(
        val context: Context,
        val state: GalleryContract.GalleryZoomViewState,
        val imageRenderer: ImageRenderer
    ) :
        PagerAdapter() {
        private var isFeatureImage = false
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun getCount(): Int {
            return state.images.size
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(context)
            val view = inflater.inflate(R.layout.item_headers_image, container, false)
            container.addView(view)
            val imageUrl = state.images[(position)].large ?: ""
            imageRenderer.render(view.itemImage, imageUrl, onResourceReadyListener = {
                view.itemImage.setImageDrawable(it)
            })
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(view)
        }
    }

}