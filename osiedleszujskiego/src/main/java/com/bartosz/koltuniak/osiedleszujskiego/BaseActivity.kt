package com.bartosz.koltuniak.osiedleszujskiego

import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.fxn.OnBubbleClickListener
import com.google.android.material.transition.platform.MaterialContainerTransformSharedElementCallback
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import kotlinx.android.synthetic.main.activity_base.*

class BaseActivity : AppCompatActivity(), Architecture.BaseActivity {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_base)
        navController = findNavController(R.id.navFragment)

        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        bottom_menu.addBubbleListener(object : OnBubbleClickListener {
            override fun onBubbleClick(itemId: Int) {
                if (itemId == R.id.action_gallery) {
                    if (navController.currentDestination?.id == R.id.postListFragment)
                        navController.navigate(R.id.posts_to_gallery)
                    if (navController.currentDestination?.id == R.id.locationFragment)
                        navController.navigate(R.id.location_gallery)
                }
                if (itemId == R.id.action_news) {
                    navController.navigate(R.id.postListFragment)
                }
                if (itemId == R.id.action_location) {
//                navController.navigate(R.id.mapsFragment)
                    if (navController.currentDestination?.id == R.id.postListFragment)
                        navController.navigate(R.id.posts_to_Location)
                    if (navController.currentDestination?.id == R.id.galleryListFragment) {
                        navController.navigate(R.id.gallery_to_Location)
                    }
                }
            }
        })
//            if (menuItem.itemId == R.id.action_location) {
////                navController.navigate(R.id.mapsFragment)
//                if (navController.currentDestination?.id == R.id.postListFragment)
//                    navController.navigate(R.id.posts_to_Location)
//                if (navController.currentDestination?.id == R.id.galleryListFragment) {
//                    navController.navigate(R.id.gallery_to_Location)
//                }
//            }
//
//        })
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.navFragment).navigateUp()
    }


//    private fun getContentTransform(): MaterialContainerTransform {
//        return MaterialContainerTransform(this).apply {
//            addTarget(android.R.id.content)
//            duration = 450
//            pathMotion = MaterialArcMotion()
//        }
//    }

    /** apply material exit container transformation. */
    fun applyExitMaterialTransform() {
        window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
        setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
        window.sharedElementsUseOverlay = false
    }

    /** apply material entered container transformation. */
    fun applyMaterialTransform(transitionName: String) {
        window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
        ViewCompat.setTransitionName(findViewById<View>(android.R.id.content), transitionName)

        // set up shared element transition
        setEnterSharedElementCallback(MaterialContainerTransformSharedElementCallback())
//        window.sharedElementEnterTransition = getContentTransform()
//        window.sharedElementReturnTransition = getContentTransform()
    }

    override fun toggleMenu(stateMenu: Architecture.StateMenu) {
        if (stateMenu == Architecture.StateMenu.HIDE) {
            bottom_menu.visibility = View.GONE
        } else {
            bottom_menu.visibility = View.VISIBLE
        }
    }
}