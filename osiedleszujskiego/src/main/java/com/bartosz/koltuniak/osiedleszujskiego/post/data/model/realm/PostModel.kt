package com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Image
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Post
import com.squareup.moshi.Json
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class PostModel(
    @PrimaryKey override var id: Long = 1,
    override var content: String = "",
    override var title: String = "",
//    @field:Json(name = "featured_image")
    override var featured_image: ImageModel? = ImageModel(),
    @field:Json(name = "media")
    var _media: RealmList<ImageModel> = RealmList()
) : RealmObject(), Post {
    override var media: List<Image>
        get() = _media
        set(value) {
            // Checks if all items are of specified implementation type
            val typeFilteredValue = value.filterIsInstance<ImageModel>()
            require(typeFilteredValue.size == value.size)
            val realmList = RealmList<ImageModel>()
            realmList.addAll(typeFilteredValue)
            _media = realmList
        }
}


open class ImageModel(
    @PrimaryKey override var id: String = "1",
    override var thumbnail: String = "",
    override var medium: String = "",
    override var large: String = ""
) : RealmObject(), Image