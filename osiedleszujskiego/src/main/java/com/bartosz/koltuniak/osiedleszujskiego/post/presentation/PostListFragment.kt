package com.bartosz.koltuniak.osiedleszujskiego.post.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.bartosz.koltuniak.osiedleszujskiego.Utils.getImageUrl
import com.bartosz.koltuniak.osiedleszujskiego.Utils.replaceFirstEmptyLine
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract.Companion.ITEM_ID
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.core.base.*
import kotlinx.android.synthetic.main.fragment_post_list.*
import kotlinx.android.synthetic.main.post_item.view.headerImage
import kotlinx.android.synthetic.main.post_item.view.title
import kotlinx.android.synthetic.main.post_item.view.description
import kotlinx.android.synthetic.main.post_item.view.skeletonGroup
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class PostListFragment :
    BaseFragment<PostContract.PostListViewState, PostContract.PostListViewModel>(),
    PostContract.PostListView {
    override val mainViewModel: PostContract.PostListViewModel by viewModel<PostListViewModel>()
    private val imageRenderer: ImageRenderer by inject()
    private val viewAdapter = BaseAdapter()
    override val stateMenu: Architecture.StateMenu = Architecture.StateMenu.SHOW
    private var sizePosts = 0

    override fun onViewStateChanged(state: PostContract.PostListViewState) {
        renderAdapter(state)
    }


    override fun init() {
        super.init()
        viewAdapter.setLayout(R.layout.post_item)
        initListAdapter()
//        val transformation: MaterialContainerTransform =
//            MaterialContainerTransform(requireContext()).apply {
//                fadeMode = MaterialContainerTransform.FADE_MODE_CROSS
//                duration = 500
//            }
//        sharedElementReturnTransition = transformation
//
//        exitTransition = Hold()
    }

    private fun initListAdapter() {
        val viewManager = LinearLayoutManager(context)

        postList.layoutManager = viewManager
        postList.setHasFixedSize(true)
        postList.adapter = viewAdapter
    }

    private fun renderAdapter(
        state: PostContract.PostListViewState
    ) {
        viewAdapter.render(object : Renderable {
            override val size: Int = state.posts.size

            override fun onItemClick(item: View, position: Int) {
                super.onItemClick(item, position)
                val extras =
                    FragmentNavigatorExtras(
                        item.headerImage to item.headerImage.transitionName
                    )
                val data = Bundle().apply {
                    putString(PostContract.ITEM_POST_HEADER, item.headerImage.transitionName)
                    putLong(ITEM_ID, state.posts[position].id)
                }

                findNavController().navigate(R.id.postDetailsFragment, data, null, extras)
            }

            override fun bindView(view: View, position: Int) {
                val post = state.posts[position]
                when {
                    state.isStartedAnimation[position] == false -> {
                        view.skeletonGroup.setShowSkeleton(false)
                    }
                    else -> {
                        view.skeletonGroup.startAnimation()

                    }
                }
                view.title.text = post.title

                view.description.text = replaceFirstEmptyLine(post.content)
                val imageUrl = getImageUrl(post)

                if (imageUrl != null)
                    imageRenderer.render(view.headerImage, imageUrl, onLoadFailedListener = {
                        view.skeletonGroup.finishAnimation()
                        view.title.visibility = View.VISIBLE
                        view.description.visibility = View.VISIBLE
                    }, onResourceReadyListener = {
                        view.skeletonGroup.finishAnimation()
                        view.headerImage.setImageDrawable(it)
                        view.title.visibility = View.VISIBLE
                        view.description.visibility = View.VISIBLE
                        mainViewModel.setFinishSkieleton(position)
                    })

                view.headerImage.transitionName = post.title
            }

        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_post_list, container, false)
    }

    override fun onDestroyView() {
        postList.adapter = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}