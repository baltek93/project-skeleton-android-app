package com.bartosz.koltuniak.osiedleszujskiego.gallery.data

import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Image
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm.ImageModel
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.bartosz.skieletonprojects.post.data.RestSzujskiegoRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Predicate
import io.reactivex.schedulers.Schedulers

class GalleryRepository(
    private val remoteRepository: RestSzujskiegoRepository,
    private val localRepository: Architecture.LocalRepository
) : GalleryContract.GalleryRepository {
    init {
        localRepository.registerTypes(Image::class.java, ImageModel::class.java)
    }

    override fun updateGallery(isPrepareData: Boolean): Completable {
        val fetchGalleries = if (isPrepareData) {
            remoteRepository.getGallery()
        } else {
            remoteRepository.getGallery(10000)
        }
        return fetchGalleries
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { listImage ->
                val finalImage = listImage.filter { it.id != "" && it.id != "0" }
                localRepository.saveData(finalImage)
            }.toCompletable()
    }


    override fun getGallery(): Observable<List<Image>> =
        localRepository.getListObservable(Image::class.java)


}