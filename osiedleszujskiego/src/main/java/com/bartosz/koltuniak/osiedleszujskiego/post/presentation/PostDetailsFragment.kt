package com.bartosz.koltuniak.osiedleszujskiego.post.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract.Companion.ITEM_ID
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.core.base.BaseFragment
import com.koltuniak.core.base.ImageRenderer
import com.skydoves.bundler.bundle
import kotlinx.android.synthetic.main.fragment_post_details.*
import kotlinx.android.synthetic.main.item_headers_image.view.*

class PostDetailsFragment :
    BaseFragment<PostContract.PostDetailsViewState, PostContract.PostDetailsViewModel>(),
    PostContract.PostDetailsView {
    override lateinit var mainViewModel: PostContract.PostDetailsViewModel
    private lateinit var imageRenderer: ImageRenderer
    override val stateMenu: Architecture.StateMenu = Architecture.StateMenu.HIDE

    override fun init() {
        super.init()
        val itemIdLong = arguments?.getLong(ITEM_ID)
        mainViewModel.setId(itemIdLong)
        imageHeaderViewPager.transitionName = arguments?.getString(PostContract.ITEM_POST_HEADER)
        tabDots.setupWithViewPager(imageHeaderViewPager, true)
        back.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val transformation: MaterialContainerTransform =
//            MaterialContainerTransform(requireContext()).apply {
//                fadeMode = MaterialContainerTransform.FADE_MODE_IN
//                duration = 500
//            }
//        sharedElementEnterTransition = transformation
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_post_details, container, false)
    }

    override fun onViewStateChanged(state: PostContract.PostDetailsViewState) {
        if (state.post != null) {
            imageHeaderViewPager.adapter =
                context?.let { PostImageHeaderPagerAdapter(it, state, imageRenderer) }
            labelTitle.text = state.post.title
            labelDescription.text = state.post.content
        }

    }


    inner class PostImageHeaderPagerAdapter(
        val context: Context,
        private val state: PostContract.PostDetailsViewState,
        private val imageRenderer: ImageRenderer
    ) :
        PagerAdapter() {
        private var isFeatureImage = false
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun getCount(): Int {
//            return state.post?.media?.size ?: 0
            return 0
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(context)
            val view = inflater.inflate(R.layout.item_headers_image, container, false)
            container.addView(view)
            val imageUrl=""
//            val imageUrl = if (isFeatureImage) {
//                if (position == 0) {
//                    state.post?.featured_image?.large ?: ""
//                } else {
//                    state.post?.media?.get((position - 1))?.large ?: ""
//                }
//            } else {
//                state.post?.media?.get((position))?.large ?: ""
//            }
            imageRenderer.render(view.itemImage, imageUrl)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(view)
        }

    }


}