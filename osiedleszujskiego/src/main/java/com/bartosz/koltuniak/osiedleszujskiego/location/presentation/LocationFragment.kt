package com.bartosz.koltuniak.osiedleszujskiego.location.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.bartosz.koltuniak.osiedleszujskiego.Utils
import com.bartosz.koltuniak.osiedleszujskiego.location.LocationContract
import com.bartosz.koltuniak.osiedleszujskiego.location.data.MyClusterRenderer
import com.bartosz.koltuniak.osiedleszujskiego.location.data.MyItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.maps.model.PolygonOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.maps.android.clustering.ClusterManager
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.core.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_location.*
import org.koin.android.viewmodel.ext.android.viewModel


class LocationFragment :
    BaseFragment<LocationContract.LocationViewState, LocationContract.LocationViewModel>(),
    LocationContract.LocationView, OnMapReadyCallback {
    override val mainViewModel: LocationContract.LocationViewModel by viewModel<LocationViewModel>()
    var map: GoogleMap? = null
    private val TAG = "UserListFragment"
    override val stateMenu: Architecture.StateMenu = Architecture.StateMenu.SHOW
    private lateinit var sheetBehavior: BottomSheetBehavior<*>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(TAG)
        }
        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_location, container, false)
        return view
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var mapViewBundle = outState.getBundle(TAG)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(TAG, mapViewBundle)
        }

        mapView?.onSaveInstanceState(mapViewBundle)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onViewStateChanged(state: LocationContract.LocationViewState) {
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        val a = LatLng(49.6145666, 20.693644)
        val lach = LatLng(49.6133302, 20.6987502)
        val pks = LatLng(49.607056, 20.7024565)
        val kolejowa = LatLng(49.6051436, 20.6955629)
        val zygmuntowa = LatLng(49.6077828, 20.6909418)
        val rectOptions = PolygonOptions()
            .add(a, lach, pks, kolejowa, zygmuntowa, a)
        context?.resources?.getColor(R.color.colorSecondary)?.let {
            val finalColor = Utils.getColorWithAlpha(it, 0.4f)
            rectOptions?.strokeColor(finalColor)
            rectOptions?.fillColor(finalColor)
        }

        val polyline: Polygon? = googleMap?.addPolygon(rectOptions)

        val builder = LatLngBounds.Builder()
        builder.include(a)
        builder.include(lach)
        builder.include(kolejowa)
        builder.include(pks)
        builder.include(zygmuntowa)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 150))



        map = googleMap
        map?.let {
            it.uiSettings.isMyLocationButtonEnabled = false
        }
        map?.let { setUpClusterer(it) }
    }


    private var clusterManager: ClusterManager<MyItem>? = null

    private fun setUpClusterer(map: GoogleMap) {
        // Position the map.
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(51.503186, 20.7024565), 10f))

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        clusterManager = ClusterManager(this.requireContext(), map)
        clusterManager!!.setRenderer(
            MyClusterRenderer(
                this.requireContext(), map,
                clusterManager
            )
        )

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        map.setOnCameraIdleListener(clusterManager)
        map.setOnMarkerClickListener(clusterManager)

        clusterManager?.setOnClusterItemClickListener {
            if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
            Toast.makeText(this.requireContext(), it.mTitle, Toast.LENGTH_LONG).show()
            true
        }

        // Add cluster items (markers) to the cluster manager.
        addItems()
    }

    private fun addItems() {

        // Set some lat/lng coordinates to start with.
        var lat = 51.503186
        var lng = 20.6024565

        // Add ten cluster items in close proximity, for purposes of this example.
        for (i in 0..9) {
            val offset = i / 60.0
            lat += offset
            lng += offset
            val offsetItem = MyItem(lat, lng)
            clusterManager!!.addItem(offsetItem)
        }
    }
}