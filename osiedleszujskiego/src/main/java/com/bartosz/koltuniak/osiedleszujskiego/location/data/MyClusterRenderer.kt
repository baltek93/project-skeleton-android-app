package com.bartosz.koltuniak.osiedleszujskiego.location.data

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator


class MyClusterRenderer(
    val context: Context?, map: GoogleMap?,
    clusterManager: ClusterManager<MyItem>?
) :
    DefaultClusterRenderer<MyItem>(context, map, clusterManager) {
    private val mClusterIconGenerator = IconGenerator(context)

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBeforeClusterItemRendered(
        item: MyItem?,
        markerOptions: MarkerOptions
    ) {
        var iconGenerator = IconGenerator(context)
        val a = context?.resources?.getDrawable(R.drawable.location_icon)
        a?.setTint(getColor(R.color.colorPrimaryDark))
        iconGenerator.setBackground(a)

        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()))

    }

    override fun onClusterItemRendered(
        clusterItem: MyItem?,
        marker: Marker
    ) {
        super.onClusterItemRendered(clusterItem, marker)
    }

//    @SuppressLint("UseCompatLoadingForDrawables")
//    override fun onBeforeClusterRendered(
//        cluster: Cluster<MyItem?>,
//        markerOptions: MarkerOptions
//    ) {
//        val clusterIcon: Drawable? = context?.resources?.getDrawable(R.drawable.ic_lens_black_24dp)
//        context?.resources?.getColor(R.color.colorSecondaryDark)?.let {
//            clusterIcon?.setColorFilter(
//                it,
//                PorterDuff.Mode.SRC_ATOP
//            )
//        }
//        mClusterIconGenerator.setBackground(clusterIcon)
//
//        //modify padding for one or two digit numbers
//        if (cluster.size < 10) {
//            mClusterIconGenerator.setContentPadding(40, 20, 0, 0)
//        } else {
//            mClusterIconGenerator.setContentPadding(30, 20, 0, 0)
//        }
//        val icon = mClusterIconGenerator.makeIcon(cluster.size.toString())
//        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
//    }

    override fun shouldRenderAsCluster(cluster: Cluster<MyItem>): Boolean {
        return cluster.size > 1
    }

    override fun getColor(clusterSize: Int): Int {
        return context?.resources?.getColor(R.color.colorPrimary) ?: super.getColor(clusterSize)
    }
}