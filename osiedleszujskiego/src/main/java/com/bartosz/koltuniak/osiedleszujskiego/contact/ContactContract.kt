package com.bartosz.koltuniak.osiedleszujskiego.contact

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Post
import com.koltuniak.bartosz.skieletonprojects.base.Architecture

interface ContactContract {

    interface ContactView : Architecture.BaseView {
    }

    interface ContactViewModel : Architecture.BaseViewModel<ContactViewState> {
    }

    data class ContactViewState(
        var firstname: String = "",
        var lastName: String = "",
        val posts: List<Post> = listOf()
    )
}