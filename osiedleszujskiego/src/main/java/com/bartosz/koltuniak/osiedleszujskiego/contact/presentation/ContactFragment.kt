
package com.bartosz.koltuniak.osiedleszujskiego.contact.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.bartosz.koltuniak.osiedleszujskiego.contact.ContactContract
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.koltuniak.core.base.BaseFragment
import com.koltuniak.core.base.injector
import org.kodein.di.direct
import org.kodein.di.generic.instance

class ContactFragment :
    BaseFragment<ContactContract.ContactViewState, ContactContract.ContactViewModel>(),
    GalleryContract.GalleryView {
    override lateinit var viewModel: ContactContract.ContactViewModel

    override fun inject(fragment: BaseFragment<ContactContract.ContactViewState, ContactContract.ContactViewModel>) {
        viewModel = injector(context!!).direct.instance(arg = fragment)

    }

    override fun init() {
        super.init()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragmet_contact, container, false)
    }

    override fun onViewStateChanged(state: ContactContract.ContactViewState) {
    }


}