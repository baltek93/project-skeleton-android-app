package com.bartosz.koltuniak.osiedleszujskiego.contact.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.bartosz.koltuniak.osiedleszujskiego.contact.ContactContract
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.koltuniak.core.base.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel

class ContactFragment :
    BaseFragment<ContactContract.ContactViewState, ContactContract.ContactViewModel>(),
    GalleryContract.GalleryView {
    override val mainViewModel: ContactContract.ContactViewModel by viewModel<ContactViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragmet_contact, container, false)
    }

    override fun onViewStateChanged(state: ContactContract.ContactViewState) {
    }


}