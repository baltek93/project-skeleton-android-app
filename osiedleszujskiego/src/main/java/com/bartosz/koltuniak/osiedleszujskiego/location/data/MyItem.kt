package com.bartosz.koltuniak.osiedleszujskiego.location.data

import com.google.android.gms.maps.model.LatLng

import com.google.maps.android.clustering.ClusterItem


class MyItem(
    val lat: Double, val lng: Double, val mTitle: String="title", val mSnippet: String="Snippet"
) : ClusterItem {


    override fun getPosition(): LatLng {
        return LatLng(lat, lng)
    }


    override fun getSnippet(): String {
        return mSnippet
    }

    override fun getTitle(): String {
        return mTitle
    }

}