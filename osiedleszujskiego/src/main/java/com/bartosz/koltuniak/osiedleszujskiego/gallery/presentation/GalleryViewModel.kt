package com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract.GalleryViewState
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.ViewModelObserver
import com.koltuniak.core.base.injector
import io.reactivex.rxkotlin.addTo
import org.kodein.di.direct
import org.kodein.di.generic.instance

class GalleryViewModel(app: Application) : BaseViewModel<GalleryViewState>(app),
    GalleryContract.GalleryViewModel {

    private val repository: GalleryContract.GalleryRepository = injector(app).direct.instance()

    init {
        repository.updateGallery().subscribeWith(ViewModelCompletableObserver(this))
            .addTo(disposables)
        repository.getGallery().subscribeWith(ViewModelObserver(this) {
            it.size
            val hashMap = hashMapOf<Int, Boolean>()
            it.forEachIndexed { index, image ->
                hashMap[index] = true
            }

            setState(getState().copy(images = it, stateLoader = hashMap))
        }).addTo(disposables)
    }

    override fun getDefaultState(): GalleryViewState =
        GalleryViewState()


}


