package com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation

import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract.GalleryViewState
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.ViewModelObserver
import io.reactivex.rxkotlin.addTo
import org.koin.core.component.inject

class GalleryViewModel() : BaseViewModel<GalleryViewState>(),
    GalleryContract.GalleryViewModel {

    private val repository: GalleryContract.GalleryRepository by inject()

    init {
        repository.updateGallery().subscribeWith(ViewModelCompletableObserver(this))
            .addTo(disposables)
        repository.getGallery().subscribeWith(ViewModelObserver(this) {
            it.size
            val hashMap = hashMapOf<Int, Boolean>()
            it.forEachIndexed { index, image ->
                hashMap[index] = true
            }

            setState(getState().copy(images = it, stateLoader = hashMap))
        }).addTo(disposables)
    }

    override fun getDefaultState(): GalleryViewState =
        GalleryViewState()


}


