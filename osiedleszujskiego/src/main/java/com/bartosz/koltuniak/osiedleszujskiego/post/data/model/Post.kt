package com.bartosz.koltuniak.osiedleszujskiego.post.data.model

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm.ImageModel
import io.realm.RealmList


interface Post {
    val id: Long
    val title: String
    val content: String
    val featured_image: Image?
    val media: List<Image>

}


interface Image {
    val id: String
    val thumbnail: String
    val medium: String
    val large: String
}
