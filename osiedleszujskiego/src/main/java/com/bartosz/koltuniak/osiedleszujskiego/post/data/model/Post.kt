package com.bartosz.koltuniak.osiedleszujskiego.post.data.model

interface Post {
    val id: Long
    val title: String
    val content: String
    val featured_image: Image?
    val media: List<Image>
}

interface Image {
    val id: String
    val thumbnail: String
    val medium: String
    val large: String
}
