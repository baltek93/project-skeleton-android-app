package com.bartosz.koltuniak.osiedleszujskiego

import android.graphics.Color
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Post
import kotlin.math.roundToInt

object Utils {
    fun getImageUrl(post: Post): String? {
        return when {
            post.featured_image != null && post.featured_image?.large != null && post.featured_image?.large!!.isNotEmpty() -> {
                post.featured_image?.large
            }
            post.media.isNotEmpty() -> {
                post.media[0].large
            }
            else -> {
                null
            }
        }?.replace("localhost", "192.168.1.19")
    }

    fun getColorWithAlpha(color: Int, ratio: Float): Int {
        val newColor: Int
        val alpha = (Color.alpha(color) * ratio).roundToInt()
        val r: Int = Color.red(color)
        val g: Int = Color.green(color)
        val b: Int = Color.blue(color)
        newColor = Color.argb(alpha, r, g, b)
        return newColor
    }

    fun replaceFirstEmptyLine(content: String): String {
        var newContent = content
        while (newContent.isNotEmpty() && newContent.substring(IntRange(0, 0)) == "\n") {
            newContent = newContent.substring(1)
        }
        return newContent
    }

}