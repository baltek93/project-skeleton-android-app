package com.bartosz.koltuniak.osiedleszujskiego.post

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Post
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import io.reactivex.Completable
import io.reactivex.Observable

interface PostContract {
    interface PostListView : Architecture.BaseView

    interface PostListViewModel : Architecture.BaseViewModel<PostListViewState> {
        fun setFinishSkieleton(position: Int)
    }

    interface PostDetailsView : Architecture.BaseView

    interface PostDetailsViewModel : Architecture.BaseViewModel<PostDetailsViewState> {
        fun setId(itemIdLong: Long?)

    }

    data class PostDetailsViewState(
        val post: Post? = null
    )

    data class PostListViewState(
        val posts: List<Post> = listOf(),
        var isStartedAnimation: HashMap<Int, Boolean> = hashMapOf()
    )

    interface PostRepository {
        fun getPosts(): Observable<List<Post>>
        fun getPost(id: Long): Observable<Post>
        fun updatePosts(isPrepareData: Boolean): Completable
    }

    companion object {
        const val ITEM_POST_HEADER = "ITEM_POST_HEADER"
        const val ITEM_ID = "itemId"
    }

}