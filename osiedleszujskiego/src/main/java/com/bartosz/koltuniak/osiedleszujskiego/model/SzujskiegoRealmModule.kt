package com.koltuniak.bartosz.skieletonprojects.data.model

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
open class SzujskiegoRealmModule