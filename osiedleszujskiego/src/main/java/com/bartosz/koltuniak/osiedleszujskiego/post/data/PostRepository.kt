package com.bartosz.koltuniak.osiedleszujskiego.post.data

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm.PostModel
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Post
import com.koltuniak.bartosz.skieletonprojects.post.data.RestSzujskiegoRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PostRepository(
    private val remoteRepository: RestSzujskiegoRepository,
    private val localRepository: Architecture.LocalRepository
) : PostContract.PostRepository {

    init {
        localRepository.registerTypes(Post::class.java, PostModel::class.java)
    }

    override fun getPosts(): Observable<List<Post>> {
        return localRepository.getListObservable(Post::class.java)
    }

    override fun getPost(id: Long): Observable<Post> {
        return localRepository.getObservable(Post::class.java, "id", id)
    }

    override fun updatePosts(isPrepareData: Boolean): Completable {
        val fetchPosts = if (isPrepareData) {
            remoteRepository.getPosts()
        } else {
            remoteRepository.getPosts(10000)
        }
        return fetchPosts
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { posts ->
//                val finalPosts = posts.filter { !(it.title == "" || it.media.isEmpty()) }
//                finalPosts.map {
//                    if (it.featured_image?.id == "" || it.featured_image?.id == "0") {
//                        it.featured_image = null
//                    }
//                    it
//                }
                localRepository.saveData(posts)
//                localRepository.saveData(finalPosts)
            }.toCompletable()
    }

}