package com.bartosz.koltuniak.osiedleszujskiego.location.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.location.LocationContract
import com.koltuniak.core.base.BaseViewModel

class LocationViewModel(app: Application) : BaseViewModel<LocationContract.LocationViewState>(app),
    LocationContract.LocationViewModel {


    init {

    }

    override fun getDefaultState(): LocationContract.LocationViewState = LocationContract.LocationViewState()


}

