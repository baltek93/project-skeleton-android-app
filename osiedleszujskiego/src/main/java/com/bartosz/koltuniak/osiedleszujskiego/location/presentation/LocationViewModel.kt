package com.bartosz.koltuniak.osiedleszujskiego.location.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.location.LocationContract
import com.koltuniak.core.base.BaseViewModel

class LocationViewModel() : BaseViewModel<LocationContract.LocationViewState>(),
    LocationContract.LocationViewModel {


    init {

    }

    override fun getDefaultState(): LocationContract.LocationViewState = LocationContract.LocationViewState()


}

