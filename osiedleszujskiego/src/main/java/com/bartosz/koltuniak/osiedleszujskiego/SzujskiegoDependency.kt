@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.bartosz.koltuniak.osiedleszujskiego

import androidx.fragment.app.Fragment
import com.bartosz.koltuniak.osiedleszujskiego.contact.ContactContract
import com.bartosz.koltuniak.osiedleszujskiego.contact.presentation.ContactViewModel
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.gallery.data.GalleryRepository
import com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation.GalleryViewModel
import com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation.GalleryZoomViewModel
import com.bartosz.koltuniak.osiedleszujskiego.location.LocationContract
import com.bartosz.koltuniak.osiedleszujskiego.location.presentation.LocationViewModel
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.bartosz.koltuniak.osiedleszujskiego.post.data.PostRepository
import com.bartosz.koltuniak.osiedleszujskiego.post.presentation.PostDetailsViewModel
import com.bartosz.koltuniak.osiedleszujskiego.post.presentation.PostListViewModel
import com.bartosz.koltuniak.osiedleszujskiego.splash.SplashContract
import com.bartosz.koltuniak.osiedleszujskiego.splash.SplashViewModel
import com.koltuniak.bartosz.skieletonprojects.data.model.SzujskiegoRealmModule
import com.koltuniak.bartosz.skieletonprojects.post.data.RestSzujskiegoRepository
import com.koltuniak.core.base.viewModelOf
import com.koltuniak.core.model.RestConfiguration
import io.realm.Realm.getDefaultModule
import io.realm.RealmConfiguration
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import retrofit2.Retrofit

val szujskiegoDependency = Kodein.Module("module name", false) {

    bind<RestConfiguration>(overrides = true) with instance(RestConfiguration(url = "http://www.osiedleszujskiego.pl/"))

    bind<RealmConfiguration>(overrides = true) with provider {
        RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .modules(
                getDefaultModule(),
                SzujskiegoRealmModule()
            )
            .build()
    }
    bind<RestSzujskiegoRepository>() with provider {
        instance<Retrofit>().create(RestSzujskiegoRepository::class.java)
    }
    bind<PostContract.PostRepository>() with provider {
        PostRepository(instance(), instance())
    }

    bind<GalleryContract.GalleryRepository>() with provider {
        GalleryRepository(instance(), instance())
    }
    bind<PostContract.PostListViewModel>() with factory { fragment: Fragment ->
        viewModelOf(fragment.requireActivity(), PostListViewModel::class.java)
    }
    bind<PostContract.PostDetailsViewModel>() with factory { fragment: Fragment ->
        viewModelOf(fragment, PostDetailsViewModel::class.java)
    }

    bind<GalleryContract.GalleryViewModel>() with factory { fragment: Fragment ->
        viewModelOf(fragment.requireActivity(), GalleryViewModel::class.java)
    }

    bind<ContactContract.ContactViewModel>() with factory { fragment: Fragment ->
        viewModelOf(fragment.requireActivity(), ContactViewModel::class.java)
    }
    bind<LocationContract.LocationViewModel>() with factory { fragment: Fragment ->
        viewModelOf(fragment.requireActivity(), LocationViewModel::class.java)
    }

    bind<SplashContract.SplashViewModel>() with factory { fragment: Fragment ->
        viewModelOf(fragment, SplashViewModel::class.java)
    }
    bind<GalleryContract.GalleryZoomViewModel>() with factory { fragment: Fragment ->
        viewModelOf(fragment.requireActivity(), GalleryZoomViewModel::class.java)
    }
}
