@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.bartosz.koltuniak.osiedleszujskiego

import com.bartosz.koltuniak.osiedleszujskiego.contact.presentation.ContactViewModel
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.gallery.data.GalleryRepository
import com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation.GalleryViewModel
import com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation.GalleryZoomViewModel
import com.bartosz.koltuniak.osiedleszujskiego.location.presentation.LocationViewModel
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.bartosz.koltuniak.osiedleszujskiego.post.data.PostRepository
import com.bartosz.koltuniak.osiedleszujskiego.post.presentation.PostDetailsViewModel
import com.bartosz.koltuniak.osiedleszujskiego.post.presentation.PostListViewModel
import com.bartosz.koltuniak.osiedleszujskiego.splash.SplashViewModel
import com.koltuniak.bartosz.skieletonprojects.data.model.SzujskiegoRealmModule
import com.koltuniak.bartosz.skieletonprojects.post.data.RestSzujskiegoRepository
import com.koltuniak.core.model.RestConfiguration
import io.realm.Realm.getDefaultModule
import io.realm.RealmConfiguration
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val szujskiegoDependency = module {

    factory<RestConfiguration>(override = true)
    {
        RestConfiguration(url = "http://www.osiedleszujskiego.pl/")
    }
    factory<RealmConfiguration>(override = true) {
        RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .modules(
                getDefaultModule(),
                SzujskiegoRealmModule()
            )
            .build()
    }
    factory<RestSzujskiegoRepository> {
        get<Retrofit>().create(RestSzujskiegoRepository::class.java)
    }
    factory<PostContract.PostRepository>() {
        PostRepository(get(), get())
    }

    factory<GalleryContract.GalleryRepository> {
        GalleryRepository(get(), get())
    }


    viewModel { PostListViewModel() }
    viewModel { PostDetailsViewModel() }

    viewModel { GalleryViewModel() }

    viewModel { ContactViewModel() }
    viewModel { LocationViewModel() }
    viewModel { SplashViewModel() }
    viewModel { GalleryZoomViewModel() }
}
