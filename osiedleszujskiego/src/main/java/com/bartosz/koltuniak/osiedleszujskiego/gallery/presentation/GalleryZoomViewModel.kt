package com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.ViewModelObserver
import io.reactivex.rxkotlin.addTo
import org.koin.core.component.inject

class GalleryZoomViewModel() : BaseViewModel<GalleryContract.GalleryZoomViewState>(),
    GalleryContract.GalleryZoomViewModel {

    private val repository: GalleryContract.GalleryRepository by inject()
    private var itemId: String? = null

    init {
        repository.updateGallery().subscribeWith(ViewModelCompletableObserver(this))
            .addTo(disposables)
        repository.getGallery().subscribeWith(ViewModelObserver(this) {
            var selectPosition = 0
            it.forEachIndexed { index, image ->
                if (image.id == itemId) {
                    selectPosition = index
                    return@forEachIndexed
                }
            }
            setState(getState().copy(images = it, selectionPosition = selectPosition))
        }).addTo(disposables)
    }

    override fun getDefaultState(): GalleryContract.GalleryZoomViewState =
        GalleryContract.GalleryZoomViewState()

    override fun setFirstSelectPosition(itemId: String?) {
        this.itemId = itemId
    }

    override fun setSelectPosition(position: Int) {
        setState(getState().copy(selectionPosition = position))
    }


}