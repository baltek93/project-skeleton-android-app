package com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.ViewModelObserver
import com.koltuniak.core.base.injector
import io.reactivex.rxkotlin.addTo
import org.kodein.di.direct
import org.kodein.di.generic.instance

class GalleryZoomViewModel(app: Application) :
    BaseViewModel<GalleryContract.GalleryZoomViewState>(app),
    GalleryContract.GalleryZoomViewModel {

    private val repository: GalleryContract.GalleryRepository = injector(app).direct.instance()
    private var itemId: String? = null

    init {
        repository.updateGallery().subscribeWith(ViewModelCompletableObserver(this))
            .addTo(disposables)
        repository.getGallery().subscribeWith(ViewModelObserver(this) {
            var selectPosition = 0
            it.forEachIndexed { index, image ->
                if (image.id == itemId) {
                    selectPosition = index
                    return@forEachIndexed
                }
            }
            setState(getState().copy(images = it, selectionPosition = selectPosition))
        }).addTo(disposables)
    }

    override fun getDefaultState(): GalleryContract.GalleryZoomViewState =
        GalleryContract.GalleryZoomViewState()

    override fun setFirstSelectPosition(itemId: String?) {
        this.itemId = itemId
    }

    override fun setSelectPosition(position: Int) {
        setState(getState().copy(selectionPosition = position))
    }


}