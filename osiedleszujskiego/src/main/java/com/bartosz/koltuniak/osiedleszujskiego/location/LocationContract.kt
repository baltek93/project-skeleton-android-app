package com.bartosz.koltuniak.osiedleszujskiego.location

import com.koltuniak.bartosz.skieletonprojects.base.Architecture

interface LocationContract {
    interface LocationView : Architecture.BaseView

    interface LocationViewModel : Architecture.BaseViewModel<LocationViewState> {

    }


    data class LocationViewState(val a :String = "")

}