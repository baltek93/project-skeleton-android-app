package com.bartosz.koltuniak.osiedleszujskiego.post.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.ViewModelObserver
import com.koltuniak.core.base.injector
import io.reactivex.rxkotlin.addTo
import org.kodein.di.direct
import org.kodein.di.generic.instance

class PostListViewModel(app: Application) : BaseViewModel<PostContract.PostListViewState>(app),
    PostContract.PostListViewModel {

    private val repository: PostContract.PostRepository = injector(app).direct.instance()

    init {
        repository.updatePosts(false)
            .subscribeWith(ViewModelCompletableObserver(this))
            .addTo(disposables)
        repository.getPosts().subscribeWith(ViewModelObserver(this)
        {
            val a = hashMapOf<Int, Boolean>()
            it.forEachIndexed { index, _ ->
                a[index] = true
            }
            setState(getState().copy(posts = it, isStartedAnimation = a))
        }).addTo(disposables)
    }

    override fun getDefaultState(): PostContract.PostListViewState =
        PostContract.PostListViewState()

    override fun setFinishSkieleton(position: Int) {
        getState().isStartedAnimation[position] = false
    }


}


