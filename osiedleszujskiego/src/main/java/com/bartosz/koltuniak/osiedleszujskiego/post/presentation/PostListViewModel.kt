package com.bartosz.koltuniak.osiedleszujskiego.post.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.ViewModelObserver
import io.reactivex.rxkotlin.addTo
import org.koin.core.component.inject

class PostListViewModel() : BaseViewModel<PostContract.PostListViewState>(),
    PostContract.PostListViewModel {

    private val repository: PostContract.PostRepository by inject()

    init {
        repository.updatePosts(false)
            .subscribeWith(ViewModelCompletableObserver(this))
            .addTo(disposables)
        repository.getPosts().subscribeWith(ViewModelObserver(this)
        {
            val a = hashMapOf<Int, Boolean>()
            it.forEachIndexed { index, _ ->
                a[index] = true
            }
            setState(getState().copy(posts = it, isStartedAnimation = a))
        }).addTo(disposables)
    }

    override fun getDefaultState(): PostContract.PostListViewState =
        PostContract.PostListViewState()

    override fun setFinishSkieleton(position: Int) {
        getState().isStartedAnimation[position] = false
    }


}


