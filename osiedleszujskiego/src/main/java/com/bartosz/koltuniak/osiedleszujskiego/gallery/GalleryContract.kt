package com.bartosz.koltuniak.osiedleszujskiego.gallery

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Image
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import io.reactivex.Completable
import io.reactivex.Observable
import java.text.FieldPosition

interface GalleryContract {
    interface GalleryView : Architecture.BaseView {

    }

    interface GalleryViewModel : Architecture.BaseViewModel<GalleryViewState> {

    }

    interface GalleryZoomView : Architecture.BaseView {

    }

    interface GalleryZoomViewModel : Architecture.BaseViewModel<GalleryZoomViewState> {
        fun setFirstSelectPosition(itemId: String?)
        fun setSelectPosition(position: Int)

    }


    interface GalleryRepository {
        fun updateGallery(isPrepareData: Boolean = false): Completable
        fun getGallery(): Observable<List<Image>>
    }

    data class GalleryViewState(
        val images: List<Image> = listOf(),
        val stateLoader: HashMap<Int, Boolean> = hashMapOf()
    )

    data class GalleryZoomViewState(
        var selectionPosition: Int = 0,
        val images: List<Image> = listOf()
    ) {
    }
}