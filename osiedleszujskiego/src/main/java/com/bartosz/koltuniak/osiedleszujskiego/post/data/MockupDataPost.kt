package com.bartosz.koltuniak.osiedleszujskiego.post.data

import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Post
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm.ImageModel
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.realm.PostModel
import io.realm.RealmList

//
//val featureImage1 = ImageModel(
//    id = 1,
//    large = "https://i.picsum.photos/id/100/1600/1200.jpg",
//    medium = "https://i.picsum.photos/id/100/1600/1200.jpg",
//    thumbnail = "https://i.picsum.photos/id/100/1600/1200.jpg"
//)
//val featureImage2 = ImageModel(
//    id = 2,
//    large = "https://i.picsum.photos/id/101/1600/1200.jpg",
//    medium = "https://i.picsum.photos/id/101/1600/1200.jpg",
//    thumbnail = "https://i.picsum.photos/id/101/1600/1200.jpg"
//)
//val post = PostModel(
//    id = 1,
//    content = "d it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting," +
//            " remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets",
//    featured_image = featureImage1,
//    title = "Title",
//    _media = RealmList(featureImage1, featureImage2)
//)
//val post2 = PostModel(
//    id = 2,
//    content = "It was popularised in the 1960s with the release of Letraset sheets",
//    featured_image = featureImage2,
//    title = "Title2",
//    _media = RealmList(featureImage2, featureImage1)
//)
//
//val images = listOf(featureImage1, featureImage2)
//val posts = listOf(post2, post)