package com.bartosz.koltuniak.osiedleszujskiego.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import androidx.navigation.fragment.findNavController
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.koltuniak.core.base.BaseFragment
import com.koltuniak.core.base.ImageRenderer
import com.koltuniak.core.base.injector
import kotlinx.android.synthetic.main.fragment_splash.*
import org.kodein.di.direct
import org.kodein.di.generic.instance

class SplashFragment :
    BaseFragment<SplashContract.SplashViewState, SplashContract.SplashViewModel>(),
    SplashContract.SplashView {
    override lateinit var viewModel: SplashContract.SplashViewModel
    private lateinit var imageRenderer: ImageRenderer
    var isNextScreen = false
    override fun inject(fragment: BaseFragment<SplashContract.SplashViewState, SplashContract.SplashViewModel>) {
        viewModel = injector(context!!).direct.instance(arg = fragment)
        imageRenderer =
            injector(fragment.requireContext()).direct.instance(arg = fragment.requireContext())
    }

    override fun init() {
        super.init()
        val fadeOut: Animation = AlphaAnimation(0f, 1f)
        fadeOut.interpolator = AccelerateInterpolator() //and this
        fadeOut.startOffset = 1000
        fadeOut.duration = 1000
        val fadeOut2: Animation = AlphaAnimation(0f, 1f).apply {
            startOffset = 1000
            duration = 1000
            interpolator = AccelerateInterpolator()
            startOffset = 500
        }

        val rotationAnimation = RotateAnimation(
            0.0f, -90.0f,
            RotateAnimation.RELATIVE_TO_SELF, 0.5f,
            RotateAnimation.RELATIVE_TO_SELF, 0.5f
        ).apply {
            repeatCount = Animation.INFINITE
            duration = 2000
            interpolator = LinearInterpolator()
        }
        rotationAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {
                if (isNextScreen) {
                    findNavController().navigate(R.id.postListFragment)
                }
            }

            override fun onAnimationEnd(p0: Animation?) {
            }

            override fun onAnimationStart(p0: Animation?) {
            }

        })
        fadeOut2.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                logo.startAnimation(rotationAnimation)
            }

            override fun onAnimationStart(p0: Animation?) {
            }

        })

        logo.startAnimation(fadeOut2)
        logo_text.startAnimation(fadeOut)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewStateChanged(state: SplashContract.SplashViewState) {
        this.isNextScreen = state.isNextScreen
    }
}
