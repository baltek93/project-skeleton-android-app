package com.bartosz.koltuniak.osiedleszujskiego

import com.koltuniak.core.base.BaseApplication
import com.koltuniak.core.baseDependency
import org.koin.android.ext.koin.androidContext
import org.koin.core.logger.EmptyLogger


class SzujskiegoApplication : BaseApplication() {

    override fun startKoin() {
        org.koin.core.context.startKoin {
            androidContext(this@SzujskiegoApplication)
            modules(listOf(baseDependency, szujskiegoDependency))
            logger(EmptyLogger())
        }
    }

}
