package com.bartosz.koltuniak.osiedleszujskiego

import com.koltuniak.bartosz.skieletonprojects.base.BaseApplication
import com.koltuniak.core.baseDependency
import org.kodein.di.Kodein


class SzujskiegoApplication : BaseApplication() {

    override var kodein: Kodein = Kodein {
        import(baseDependency)
        import(szujskiegoDependency, allowOverride = true)
    }

}
