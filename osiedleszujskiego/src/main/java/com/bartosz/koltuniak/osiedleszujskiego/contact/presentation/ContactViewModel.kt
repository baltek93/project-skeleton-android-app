package com.bartosz.koltuniak.osiedleszujskiego.contact.presentation

import com.bartosz.koltuniak.osiedleszujskiego.contact.ContactContract
import com.koltuniak.core.base.BaseViewModel

class ContactViewModel()  : BaseViewModel<ContactContract.ContactViewState>(),
    ContactContract.ContactViewModel {

    init {
    }

    override fun getDefaultState(): ContactContract.ContactViewState =
        ContactContract.ContactViewState()


}


