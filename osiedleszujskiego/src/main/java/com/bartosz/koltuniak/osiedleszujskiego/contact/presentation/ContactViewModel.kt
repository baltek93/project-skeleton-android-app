package com.bartosz.koltuniak.osiedleszujskiego.contact.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.contact.ContactContract
import com.koltuniak.core.base.BaseViewModel

class ContactViewModel(app:Application)  : BaseViewModel<ContactContract.ContactViewState>(app),
    ContactContract.ContactViewModel {

    init {
    }

    override fun getDefaultState(): ContactContract.ContactViewState =
        ContactContract.ContactViewState()


}


