package com.bartosz.koltuniak.osiedleszujskiego.post.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.ViewModelObserver
import io.reactivex.rxkotlin.addTo
import org.koin.core.component.inject

class PostDetailsViewModel() :
    BaseViewModel<PostContract.PostDetailsViewState>(),
    PostContract.PostDetailsViewModel {

    private val repository: PostContract.PostRepository by inject()

    init {
//        repository.updatePosts()
//            .subscribeWith(ViewModelCompletableObserver(this))
//            .addTo(disposables)

    }

    override fun getDefaultState(): PostContract.PostDetailsViewState =
        PostContract.PostDetailsViewState()

    override fun setId(itemIdLong: Long?) {
        if (itemIdLong != null)
            repository.getPost(itemIdLong).subscribeWith(ViewModelObserver(this)
            {
                setState(getState().copy(post = it))
            }).addTo(disposables)
    }


}