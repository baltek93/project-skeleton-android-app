package com.bartosz.koltuniak.osiedleszujskiego.post.presentation

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelObserver
import com.koltuniak.core.base.injector
import io.reactivex.rxkotlin.addTo
import org.kodein.di.direct
import org.kodein.di.generic.instance

class PostDetailsViewModel(app: Application) :
    BaseViewModel<PostContract.PostDetailsViewState>(app),
    PostContract.PostDetailsViewModel {

    private val repository: PostContract.PostRepository = injector(app).direct.instance()

    init {
//        repository.updatePosts()
//            .subscribeWith(ViewModelCompletableObserver(this))
//            .addTo(disposables)

    }

    override fun getDefaultState(): PostContract.PostDetailsViewState =
        PostContract.PostDetailsViewState()

    override fun setId(itemIdLong: Long?) {
        if (itemIdLong != null)
            repository.getPost(itemIdLong).subscribeWith(ViewModelObserver(this)
            {
                setState(getState().copy(post = it))
            }).addTo(disposables)
    }


}