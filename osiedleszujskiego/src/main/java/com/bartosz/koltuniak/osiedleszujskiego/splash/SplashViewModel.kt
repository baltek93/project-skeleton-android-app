package com.bartosz.koltuniak.osiedleszujskiego.splash

import android.app.Application
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import com.koltuniak.core.base.injector
import io.reactivex.rxkotlin.addTo
import org.kodein.di.direct
import org.kodein.di.generic.instance
import java.util.concurrent.TimeUnit

class SplashViewModel(val app: Application) : BaseViewModel<SplashContract.SplashViewState>(app),
    SplashContract.SplashViewModel {

    private val postRepository: PostContract.PostRepository = injector(app).direct.instance()
    private val galleryRepository: GalleryContract.GalleryRepository = injector(app).direct.instance()

    init {
        postRepository.updatePosts(true).timeout(4, TimeUnit.SECONDS)
            .andThen(galleryRepository.updateGallery(true)).timeout(4, TimeUnit.SECONDS)
            .subscribeWith(ViewModelCompletableObserver(this, {
                setState(getState().copy(isNextScreen = true))
            }, {
                setState(getState().copy(isNextScreen = true))
            })).addTo(disposables)
    }

    override fun getDefaultState(): SplashContract.SplashViewState =
        SplashContract.SplashViewState()

}