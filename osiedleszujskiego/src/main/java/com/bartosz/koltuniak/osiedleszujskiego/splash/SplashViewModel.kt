package com.bartosz.koltuniak.osiedleszujskiego.splash

import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.koltuniak.core.base.BaseViewModel
import com.koltuniak.core.base.ViewModelCompletableObserver
import io.reactivex.rxkotlin.addTo
import org.koin.java.KoinJavaComponent.inject
import java.util.concurrent.TimeUnit

class SplashViewModel() : BaseViewModel<SplashContract.SplashViewState>(),
    SplashContract.SplashViewModel {

    private val postRepository: PostContract.PostRepository by inject(PostContract.PostRepository::class.java)
    private val galleryRepository: GalleryContract.GalleryRepository by inject(GalleryContract.GalleryRepository::class.java)

    init {
        postRepository.updatePosts(true).timeout(4, TimeUnit.SECONDS)
            .andThen(galleryRepository.updateGallery(true)).timeout(4, TimeUnit.SECONDS)
            .subscribeWith(ViewModelCompletableObserver(this, {
                setState(getState().copy(isNextScreen = true))
            }, {
                setState(getState().copy(isNextScreen = true))
            })).addTo(disposables)
    }

    override fun getDefaultState(): SplashContract.SplashViewState =
        SplashContract.SplashViewState()

}