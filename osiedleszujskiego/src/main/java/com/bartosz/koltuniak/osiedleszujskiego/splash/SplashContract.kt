package com.bartosz.koltuniak.osiedleszujskiego.splash

import com.koltuniak.bartosz.skieletonprojects.base.Architecture

interface SplashContract {
    interface SplashView : Architecture.BaseView

    interface SplashViewModel : Architecture.BaseViewModel<SplashViewState>

    data class SplashViewState(val isNextScreen: Boolean = false)
}