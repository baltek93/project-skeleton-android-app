package com.bartosz.koltuniak.osiedleszujskiego.gallery.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bartosz.koltuniak.osiedleszujskiego.R
import com.bartosz.koltuniak.osiedleszujskiego.gallery.GalleryContract
import com.bartosz.koltuniak.osiedleszujskiego.post.PostContract
import com.bartosz.koltuniak.osiedleszujskiego.post.data.model.Image
import com.koltuniak.bartosz.skieletonprojects.base.Architecture
import com.koltuniak.core.base.*
import kotlinx.android.synthetic.main.fragment_gallery_list.*
import kotlinx.android.synthetic.main.gallery_item.view.*
import org.kodein.di.direct
import org.kodein.di.generic.instance

class GalleryListFragment :
    BaseFragment<GalleryContract.GalleryViewState, GalleryContract.GalleryViewModel>(),
    GalleryContract.GalleryView {
    override lateinit var viewModel: GalleryContract.GalleryViewModel
    private val viewAdapter = BaseAdapter()
    lateinit var imageRenderer: ImageRenderer
    override val stateMenu: Architecture.StateMenu = Architecture.StateMenu.SHOW

    override fun inject(fragment: BaseFragment<GalleryContract.GalleryViewState, GalleryContract.GalleryViewModel>) {
        viewModel = injector(context!!).direct.instance(arg = fragment)
        imageRenderer =
            injector(fragment.requireContext()).direct.instance(arg = fragment.requireContext())
    }

    override fun init() {
        super.init()
        viewAdapter.setLayout(R.layout.gallery_item)
        initListAdapter()
    }

    private fun initListAdapter() {
        val viewManager = StaggeredGridLayoutManager(2, 1)
        galleryList.layoutManager = viewManager
        galleryList.adapter = viewAdapter
        renderList(emptyList())
    }

    private fun renderList(images: List<Image>) {
        viewAdapter.render(object : Renderable {
            override val size = images.size

            override fun onItemClick(item: View, position: Int) {
                val image = images[position]
                val imageUrl = image.large.replace("localhost", "192.168.1.19")
                val data = Bundle().apply {
                    putString(PostContract.ITEM_ID, image.id)
                }

                findNavController().navigate(R.id.gallery_to_galleryZoom, data)
            }

            override fun bindView(view: View, position: Int) {
                val image = images[position]
                val imageUrl = image.thumbnail.replace("localhost", "192.168.1.19")

                imageRenderer.render(view.headerImage, imageUrl, onLoadFailedListener = {
                    view.skeletonGroup.finishAnimation()
                }, onResourceReadyListener = {
                    view.skeletonGroup.finishAnimation()
                    view.headerImage.setImageDrawable(it)
                })
            }

        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_gallery_list, container, false)
    }


    override fun onViewStateChanged(state: GalleryContract.GalleryViewState) {
        renderList(state.images)
    }

    override fun onDestroyView() {
        galleryList.adapter = null
        super.onDestroyView()
    }


}